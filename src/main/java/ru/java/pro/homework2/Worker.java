package ru.java.pro.homework2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Worker {
    String name;
    Integer age;
    Position position;

    public Worker(String name, Integer age, Position position) {
        this.name = name;
        this.age = age;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", position=" + position +
                '}';
    }

    public enum Position {
        ENGINEER,
        MANAGER,
        HANDYMAN
    }

    public static List<Integer> deleteDuplicate(List<Integer> list) {
        return list.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public static Integer thirdBiggestNumber(List<Integer> list) {
        return list.stream()
                .sorted(Comparator.reverseOrder())
                .skip(2)
                .findFirst()
                .orElse(0);
    }

    public static Integer thirdBiggestUniqueNumber(List<Integer> list) {
        return list.stream()
                .distinct()
                .sorted(Comparator.reverseOrder())
                .skip(2)
                .findFirst()
                .orElse(0);
    }

    public static String findBiggestWord(List<String> list) {
        return list.stream()
                .max(Comparator.comparing(String::length))
                .orElse("");
    }

    public static List<Worker> getListNamesOldestEngineers(List<Worker> list) {
        return list.stream()
                .filter(w -> w.position == Position.ENGINEER)
                .sorted(Comparator.comparing(Worker::getAge).reversed())
                .limit(3)
                .collect(Collectors.toList());
    }

    public static Double getAverageAgeOfWorkers(List<Worker> list) {
        return list.stream()
                .filter(w -> w.position == Position.ENGINEER)
                .sorted(Comparator.comparing(Worker::getAge).reversed())
                .mapToInt(Worker::getAge)
                .average()
                .orElse(0);
    }

    public static Map<String, Long> getMapWordToCountWords(String words) {
        /*Альтернативный способ решения
         return Arrays.stream(words.split(" "))
         .distinct().collect(
         Collectors.toMap(a->a,b-> (int) Arrays
         .stream(words.split(" "))
         .filter(f -> f.equals(b)).count()))
         */

        return Arrays.stream(words.split(" "))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    public static List<String> getLongWords(List<String> listWord) {
        return listWord.stream()
                .sorted(Comparator
                        .comparing(String::length)
                        .thenComparing(Comparator.naturalOrder()))
                .collect(Collectors.toList());
    }

    public static String getLongestWord(List<String> wordsList) {
        return wordsList.stream()
                .flatMap(words1 -> Arrays.stream(words1.split(" ")))
                .distinct()
                .max(Comparator.comparing(String::length))
                .orElse("");
    }
}