package ru.java.pro.homework3;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadsPool {
    private final int countOfThreads;
    private static int poolNumber = 0;
    private final List<Runnable> listCommands;
    private final List<Thread> listThreads;
    private boolean shutdownDisabled = true;
    private final String poolName;
    private final Lock lock1 = new ReentrantLock();
    private final Object monitor = new Object();
    private Runnable command = null;
    private Runnable thread = () -> {
        while (shutdownDisabled) {
            Boolean runnable = true;
            Runnable commandRun = null;
            while (runnable) {
                synchronized (monitor) {
                    while (command == null) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    runnable = false;
                    commandRun = command;
                    command = null;
                    monitor.notifyAll();
                }
            }
            commandRun.run();
        }
    };

    private void prnt(String s) {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy-HH:mm:SS"))
                + " " + poolName + "-" + Thread.currentThread().getName().toLowerCase() + " " + s);
    }

    public ThreadsPool(int countOfThreads) {
        this.countOfThreads = countOfThreads;
        this.poolName = "pool-" + ++poolNumber;
        this.listCommands = new LinkedList<Runnable>();
        this.listThreads = new ArrayList<Thread>();

        for (int i = 0; i < countOfThreads; i++) {
            listThreads.add(new Thread(thread));
        }

        for (Thread t : listThreads
        ) {
            t.start();
        }

        Thread queueHandlerThread = new Thread(() -> {
            while (shutdownDisabled) {
                try {
                    lock1.lock();
                    while (listCommands.size() != 0) {
                        synchronized (monitor) {
                            while (command != null) {
                                try {
                                    monitor.wait();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            command = listCommands.get(0);
                            listCommands.remove(command);
                            monitor.notifyAll();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    lock1.unlock();
                }
            }
        }
        );
        queueHandlerThread.setDaemon(true);
        queueHandlerThread.start();
    }

    public void execute(Runnable r) {
        if (shutdownDisabled) {
            try {
                lock1.lock();
                if (listCommands.size() == 0 && command == null) {
                    synchronized (monitor) {
                        command = r;
                        monitor.notifyAll();
                        return;
                    }
                }
                listCommands.add(r);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock1.unlock();
            }
        } else throw new IllegalStateException("Очередь остановлена");
    }

    public void shutdown() {
        synchronized (monitor) {
            command = null;
            shutdownDisabled = false;
        }
    }

    public Runnable work(String s, Long l) {
        return () -> {
            prnt(s);
            try {
                Thread.sleep(l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            prnt(s + " работать закончил");
        };
    }
}



