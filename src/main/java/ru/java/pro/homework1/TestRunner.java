package ru.java.pro.homework1;

import ru.java.pro.homework1.annotation.AfterSuite;
import ru.java.pro.homework1.annotation.BeforeSuite;
import ru.java.pro.homework1.annotation.CsvSource;
import ru.java.pro.homework1.annotation.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class TestRunner {
    public static void runTests(Class c) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        Method[] methods = c.getDeclaredMethods();
        Method beforeSuiteMethod = null;
        Method afterSuiteMethod = null;
        int testMethodsCount = 0;
        for (Method method : methods) {

            //Проверка аннотации BeforeSuite
            if (method.isAnnotationPresent(BeforeSuite.class)) {
                if (beforeSuiteMethod != null) {
                    System.out.println("В классе более одного метода с аннотацией BeforeSuite");
                    assert false;
                }

                if (!Modifier.isStatic(method.getModifiers())) {
                    System.out.println("Метод " + method.getName() + " не является статическим");
                    assert false;
                }

                beforeSuiteMethod = method;
            }

            //Проверка аннотации AfterSuite
            if (method.isAnnotationPresent(AfterSuite.class)) {
                if (afterSuiteMethod != null) {
                    System.out.println("В классе более одного метода с аннотацией AfterSuite");
                    assert false;
                }

                if (!Modifier.isStatic(method.getModifiers())) {
                    System.out.println("Метод " + method.getName() + " не является статическим");
                    assert false;
                }

                afterSuiteMethod = method;
            }

            //Проверка аннотации Test + подсчет количества тестовых методов
            if (method.isAnnotationPresent(Test.class)) {
                if (Modifier.isStatic(method.getModifiers())) {
                    System.out.println("Метод " + method.getName() + " с аннотацией Test является статическим");
                    assert false;
                }

                Test testMethod = method.getAnnotation(Test.class);
                if (testMethod.priority() < 1 || testMethod.priority() > 10) {
                    System.out.println("В методе " + method.getName() + " с аннотацией Test приоритет " + testMethod.priority() + " выходит за интервал 1..10");
                    assert false;
                }
                testMethodsCount += 1;
            }

            if (method.isAnnotationPresent(CsvSource.class)) {

                if (!method.isAnnotationPresent(Test.class)) {
                    System.out.println("Метод " + method.getName() + " с аннотацией CsvSource не является тестовым методом");
                    assert false;
                }

                CsvSource csvSourceAnnotation = method.getAnnotation(CsvSource.class);
                if (csvSourceAnnotation.params().isEmpty()) {
                    System.out.println("В методе " + method.getName() + " с аннотацией CsvSource пустой список параметров в аннотации");
                    assert false;
                }

                if (method.getParameterCount() == 0) {
                    System.out.println("В методе " + method.getName() + " с аннотацией CsvSource пустой список параметров в методе");
                    assert false;
                }
            }
        }

        //Создание массива тестовых методов
        Method[] testMethods = new Method[testMethodsCount];
        int i = 0;
        for (Method method : methods) {
            if (method.isAnnotationPresent(Test.class)) {
                testMethods[i] = method;
                i += 1;
            }
        }

        //Выполнение BeforeSuite метода
        if (beforeSuiteMethod != null) {
            beforeSuiteMethod.invoke(null);
        }

        //Сортировка тестовых методов по приоритету и выполнение
        sortByPriority(testMethods);
        for (Method method : testMethods) {
            if (method.isAnnotationPresent(CsvSource.class)) {
                var parameterTypesFromTestMethod = method.getParameterTypes();
                Object[] parametersFromCsvSource = method.getAnnotation(CsvSource.class).params().split(method.getAnnotation(CsvSource.class).separator());

                if (parameterTypesFromTestMethod.length != parametersFromCsvSource.length) {
                    System.out.println("В методе " + method.getName() + " с аннотацией CsvSource количество параметров в аннотации не совпадает с количеством параметров в методе");
                    assert false;
                }

                Object[] parameterFromCsvSourceAfterParse = new Object[parameterTypesFromTestMethod.length];

                for (int j = 0; j < parameterTypesFromTestMethod.length; j++) {
                    parameterFromCsvSourceAfterParse[j] = parse(parameterTypesFromTestMethod[j].getSimpleName(), (String) parametersFromCsvSource[j]);
                }

                try {
                    method.invoke(c.getDeclaredConstructor().newInstance(), parameterFromCsvSourceAfterParse);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else {
                method.invoke(c.getDeclaredConstructor().newInstance());
            }
        }

        //Выполнение AfterSuite метода
        if (afterSuiteMethod != null) {
            afterSuiteMethod.invoke(null);
        }

    }

    public static Object parse(String type, String str) {
        Object result = null;
        switch (type) {
            case "int":
                result = Integer.valueOf(str).intValue();
                break;
            case "byte":
                result = Byte.valueOf(str).byteValue();
                break;
            case "short":
                result = Short.valueOf(str).shortValue();
                break;
            case "long":
                result = Long.valueOf(str).longValue();
                break;
            case "float":
                result = Float.valueOf(str).floatValue();
                break;
            case "double":
                result = Double.valueOf(str).doubleValue();
                break;
            case "char":
                result = str.charAt(0);
                break;
            case "boolean":
                result = Boolean.valueOf(str).booleanValue();
                break;
            case "String":
                result = str;
                break;
            case "Integer":
                result = Integer.valueOf(str);
                break;
            case "Byte":
                result = Byte.valueOf(str);
                break;
            case "Short":
                result = Short.valueOf(str);
                break;
            case "Long":
                result = Long.valueOf(str);
                break;
            case "Float":
                result = Float.valueOf(str);
                break;
            case "Double":
                result = Double.valueOf(str);
                break;
            case "Character":
                result = Character.valueOf(str.charAt(0));
                break;
            case "Boolean":
                result = Boolean.valueOf(str);
                break;
            default:
                System.out.println("Тип " + type + " не входит в список разрешенных для указания в CsvSource");
                assert false;
        }
        ;
        return result;
    }

    public static void sortByPriority(Method[] methods) {
        Method temp;
        for (int i = 0; i < methods.length; i++) {
            for (int j = 0; j < methods.length - 1 - i; j++) {
                if (methods[j].getAnnotation(Test.class).priority() < methods[j + 1].getAnnotation(Test.class).priority()) {
                    temp = methods[j];
                    methods[j] = methods[j + 1];
                    methods[j + 1] = temp;
                }
            }
        }
    }
}
