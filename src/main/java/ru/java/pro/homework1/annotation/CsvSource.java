package ru.java.pro.homework1.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
/*
В params() есть возможность указать такие типы как:
примитивы, их обёртки и String
* */
public @interface CsvSource {
    String params() default "";
    String separator() default ", ";
}
