package ru.java.pro.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {
    public static void prnt(String s){
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy-HH:mm:SS")) + " " + Thread.currentThread().getName().toLowerCase() + " " + s);
    }
}
