package ru.java.pro.homework2;

import ru.java.pro.homework1.TestBox;
import ru.java.pro.homework1.annotation.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.java.pro.homework1.TestRunner.runTests;
import static ru.java.pro.homework2.Worker.*;

public class TestWorker {

    @Test
    public void firstTest(){
        System.out.println("Удаление дубликатов");
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 6, 6, 7, 8, 8, 9);
        System.out.println(list);
        System.out.println(deleteDuplicate(list));
        System.out.println("--------------------");
    }
    @Test
    public void secondTest(){
        System.out.println("3-е наибольшее число");
        List<Integer> list = Arrays.asList(1, 2, 2, 8, 5, 5, 6, 6, 7, 8, 8, 9);
        System.out.println(list);
        System.out.println(thirdBiggestNumber(list));
        System.out.println("--------------------");
    }
    @Test
    public void thirdTest(){
        System.out.println("3-е наибольшее уникальное число");
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 6, 6, 7, 8, 8, 9);
        System.out.println(list);
        System.out.println(thirdBiggestUniqueNumber(list));
        System.out.println("--------------------");
    }
    @Test
    public void fourthTest(){
        System.out.println("3-е самых старых инженера");
        List<Worker> workers = new ArrayList<>(List.of());
        workers.add(new Worker("Andrew", 20, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew1", 40, Worker.Position.HANDYMAN));
        workers.add(new Worker("Andrew2", 32, Worker.Position.MANAGER));
        workers.add(new Worker("Andrew3", 64, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew4", 35, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew5", 19, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew6", 28, Worker.Position.ENGINEER));
        System.out.println(workers);
        System.out.println(getListNamesOldestEngineers(workers));
        System.out.println("--------------------");
    }
    @Test
    public void fifthTest(){
        System.out.println("Средний возраст инженеров");
        List<Worker> workers = new ArrayList<>(List.of());
        workers.add(new Worker("Andrew", 20, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew1", 40, Worker.Position.HANDYMAN));
        workers.add(new Worker("Andrew2", 32, Worker.Position.MANAGER));
        workers.add(new Worker("Andrew3", 64, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew4", 35, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew5", 19, Worker.Position.ENGINEER));
        workers.add(new Worker("Andrew6", 28, Worker.Position.ENGINEER));
        System.out.println(workers);
        System.out.println(getAverageAgeOfWorkers(workers));
        System.out.println("--------------------");
    }
    @Test
    public void sixthTest(){
        System.out.println("Самое длинное слово");
        List<String> listWord = Arrays.asList("duck", "duck1", "duck12", "auck12", "duck1234", "duck123");
        System.out.println(listWord);
        System.out.println(findBiggestWord(listWord));
        System.out.println("--------------------");
    }
    @Test
    public void seventhTest(){
        System.out.println("Пары слово-количество");
        String words = "принц принцесса король королева царство принц царство король король король королевство";
        System.out.println(words);
        System.out.println(getMapWordToCountWords(words));
        System.out.println("--------------------");
    }
    @Test
    public void eighthTest(){
        System.out.println("Порядок слов по длинне");
        List<String> listWord = Arrays.asList("duck", "duck1", "duck12", "auck12", "duck1234", "duck123");
        System.out.println(listWord);
        System.out.println(getLongWords(listWord));
        System.out.println("--------------------");
    }
    @Test
    public void ninthTest(){
        System.out.println("Самое длинное слово из списка");
        List<String> wordsList = Arrays.asList("принц принцесса", "король королева царство", "принц царство король король", "король королевство");
        System.out.println(wordsList);
        System.out.println(getLongestWord(wordsList));
        System.out.println("--------------------");
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        runTests(TestWorker.class);
    }
}
