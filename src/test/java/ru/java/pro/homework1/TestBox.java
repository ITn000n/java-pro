package ru.java.pro.homework1;

import ru.java.pro.homework1.annotation.AfterSuite;
import ru.java.pro.homework1.annotation.BeforeSuite;
import ru.java.pro.homework1.annotation.CsvSource;
import ru.java.pro.homework1.annotation.Test;

import java.lang.reflect.InvocationTargetException;

import static ru.java.pro.homework1.TestRunner.runTests;

public class TestBox {
    @BeforeSuite
    public static void beforeSuiteMethod() {
        System.out.println("beforeSuiteMethod execute");
    }

    @AfterSuite
    public static void afterSuiteMethod() {
        System.out.println("afterSuiteMethod execute");
    }

    @Test(priority = 2)
    public void m1() {
        System.out.println("m1");
    }

    @Test(priority = 4)
    public void m2() {
        System.out.println("m2");
    }

    @Test(priority = 10)
    public void m3() {
        System.out.println("m3");
    }

    @Test()
    public void m4() {
        System.out.println("m4");
    }

    @Test()
    public void m5() {
        System.out.println("m5");
    }

    @Test(priority = 1)
    public void m6() {
        System.out.println("m6");
    }

    @Test()
    @CsvSource(params = "10, Java, 20, true")
    public void csvTest(Integer a, String b, int c, boolean d) {
        System.out.println(a + " " + b + " " + c + " " + d);
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        runTests(TestBox.class);
    }
}
