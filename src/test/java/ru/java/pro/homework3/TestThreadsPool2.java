package ru.java.pro.homework3;

import ru.java.pro.homework1.annotation.Test;
import ru.java.pro.utils.Utils;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Executors;

import static ru.java.pro.homework1.TestRunner.runTests;

public class TestThreadsPool2 {

   

    @Test
    public void secondTest() throws InterruptedException {
        var rr = Executors.newFixedThreadPool(4);

        rr.execute(put(" 1 тест", 5000L));
        rr.execute(put(" 2 тест", 5000L));
        rr.execute(put(" 3 тест", 5000L));
        rr.execute(put(" 4 тест", 5000L));
        rr.execute(put(" 5 тест", 5000L));
        for (int i = 0; i < 100; i++) {

            Utils.prnt("Выполняется задача основного потока " + i);
            Thread.sleep(200);
            if (i == 50) {
                rr.execute(put(" 9 тест", 5000L));
                rr.execute(put(" 10 тест", 5000L));
                rr.execute(put(" 11 тест", 5000L));
                rr.execute(put(" 12 тест", 5000L));
            }
        }

        rr.execute(put("6 тест", 250L));
        rr.execute(put("7 тест", 250L));
        rr.execute(put("8 тест", 250L));

        for (int i = 0; i < 100; i++) {
            Utils.prnt("Выполняется задача основного потока " + i);
            Thread.sleep(200);
            if (i == 50) {
                rr.execute(put(" 13 тест", 5000L));
                rr.execute(put(" 14 тест", 5000L));
                rr.execute(put(" 15 тест", 5000L));
                rr.execute(put(" 16 тест", 5000L));
                rr.execute(put(" 17 тест", 5000L));
                rr.execute(put(" 18 тест", 5000L));
                rr.execute(put(" 19 тест", 5000L));
                rr.execute(put(" 20 тест", 5000L));
                rr.execute(put(" 21 тест", 5000L));
                rr.execute(put(" 22 тест", 5000L));
            }
        }
        rr.shutdownNow();
        rr.execute(put("Финальный тест", 250L));
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        runTests(TestThreadsPool2.class);
    }

    public Runnable put(String s, Long l) {
        return () -> {
            System.out.println(Thread.currentThread().getName() + s);
            try {
                Thread.sleep(l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + s + " работать закончил");
        };
    }
}
