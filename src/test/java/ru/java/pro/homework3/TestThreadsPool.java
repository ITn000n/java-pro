package ru.java.pro.homework3;

import ru.java.pro.homework1.annotation.Test;
import ru.java.pro.utils.Utils;

import java.lang.reflect.InvocationTargetException;

import static ru.java.pro.homework1.TestRunner.runTests;

public class TestThreadsPool {

    @Test
    public void firstTest() throws InterruptedException {

        ThreadsPool threadsPool = new ThreadsPool(3);

        threadsPool.execute(threadsPool.work(" 1 тест", 5000L));
        threadsPool.execute(threadsPool.work(" 2 тест", 5000L));
        threadsPool.execute(threadsPool.work(" 3 тест", 5000L));
        threadsPool.execute(threadsPool.work(" 4 тест", 5000L));
        threadsPool.execute(threadsPool.work(" 5 тест", 5000L));


        for (int i = 0; i < 100; i++) {

            Utils.prnt("Выполняется задача основного потока " + i);
            Thread.sleep(200);
            if (i == 50) {
                threadsPool.execute(threadsPool.work(" 9 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 10 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 11 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 12 тест", 5000L));
            }
        }
        threadsPool.execute(threadsPool.work("6 тест", 250L));
        threadsPool.execute(threadsPool.work("7 тест", 250L));
        threadsPool.execute(threadsPool.work("8 тест", 250L));

        for (int i = 0; i < 100; i++) {
            Utils.prnt("Выполняется задача основного потока " + i);
            Thread.sleep(200);
            if (i == 50) {
                threadsPool.execute(threadsPool.work(" 13 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 14 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 15 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 16 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 17 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 18 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 19 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 20 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 21 тест", 5000L));
                threadsPool.execute(threadsPool.work(" 22 тест", 5000L));
            }
        }
        // rr.shutdownNow();
        threadsPool.shutdown();
        threadsPool.execute(threadsPool.work("Финальный тест", 250L));
    }


    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        runTests(TestThreadsPool.class);
    }
}
